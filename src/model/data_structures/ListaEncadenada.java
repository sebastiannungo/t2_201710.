package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;
	private int size=0;
	private NodoSencillo<T> ref;

	
	public NodoSencillo<T> darCabeza(){
		return primero;
	}
	public Iterator<T> iterator() {

		return new Iterator<T>(){
			NodoSencillo<T> act=null;
			@Override
			public boolean hasNext() {
				if(size==0)
				{
					return false;
				}
				if(act==null)
					return true;

				return act.getSiguiente() != null;

			}

			@Override
			public T next() {

				return (T) act.getSiguiente().darElemento();
			}

		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(size==0)
		{
			primero = new NodoSencillo(elem);
			ref=primero;
			size++;
		}
		else
		{
			NodoSencillo actual=primero;
			NodoSencillo nodoEntrante=new NodoSencillo(elem);
			for (int i = 0; i < size-1; i++) {
				actual=actual.getSiguiente();
			}

			actual.addSiguiente(nodoEntrante);
		}
		size++;

	}

	@Override
	public T darElemento(int pos) {
		T elemento=null;
		if(pos>size)
		{
			throw new OutOfMemoryError();
		}
		else
		{
			NodoSencillo actual=primero;
			for (int i = 0; i < pos; i++) {
				actual=actual.getSiguiente();
			}
			elemento=(T) actual.darElemento();
		}
		return elemento;

	}


	@Override
	public int darNumeroElementos() {

		return size+1;
	}

	@Override
	public T darElementoPosicionActual() {
		return (T) ref.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(ref.getSiguiente()!=null)
		{
			ref=ref.getSiguiente();
			return true;
		}

		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		NodoSencillo actual=primero;

		NodoSencillo anterior=null;
		for (int i = 0; i < size; i++) {
			if(actual.getSiguiente()==ref&&actual.getSiguiente()!=null)
			{
				anterior=actual;
				break;
			}
			actual=actual.getSiguiente();
		}
		if(anterior!=null)
		{
			ref=anterior;

		}
		return anterior!=null;
	}

}
