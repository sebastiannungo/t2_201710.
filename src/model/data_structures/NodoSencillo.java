package model.data_structures;


public class NodoSencillo<T> {
	private T elemento;

	private NodoSencillo siguiente;
	public NodoSencillo(T parEle)
	{
		elemento = parEle;
		siguiente=null;
	}
	public void addSiguiente(NodoSencillo sig)
	{
		 siguiente= sig;
	}
	public void setSiguiente(NodoSencillo parSig)
	{
		siguiente = parSig;
	}
	public Object darElemento()
	{
		return elemento;
	}
	public NodoSencillo getSiguiente()
	{
		return siguiente;
	}
	
}
