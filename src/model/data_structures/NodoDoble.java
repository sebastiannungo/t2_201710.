package model.data_structures;

public class NodoDoble<E> {
	
	private NodoDoble<E> siguiente;
	
	private NodoDoble<E> anterior;
	
	private E hijo;
	
	public NodoDoble(E item){
		siguiente = null;
		anterior = null;
		hijo = item;
	}
	
	public NodoDoble getSiguiente(){
		return siguiente;
	}
	
	public NodoDoble getAnterior (){
		return anterior;
	}
	
	public Object getHijo(){
		return hijo;
	}
	
	public void setSiguiente(NodoDoble p){
		siguiente = p;
	}
	
	public void setAnterior(NodoDoble p){
		anterior = p;
	}
	
	public void setHijo(E p){
		hijo = p;
	}
	
	
}
