package listasTests;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.NodoDoble;

public class ListaDoblementeEncadenadaTest extends TestCase {
	
	private ListaDobleEncadenada<String> listaDoble;
	
	public void setupEscenario1(){
		
		try{
			
		listaDoble = new ListaDobleEncadenada<String>();
		listaDoble.agregarElementoFinal("A");
		listaDoble.agregarElementoFinal("B");
		listaDoble.agregarElementoFinal("C");
		} catch(Exception e){
			fail("No se deberia genrear error" + "-----" +  e.getMessage());
		}
		
	}
	
	public void setupEscenario2(){
		try {
			listaDoble = new ListaDobleEncadenada<String>();
		}catch(Exception e){
			//
		}
	}
	
	 /**
     * Prueba 1: Se encarga de verificar el m�todo constructor de la clase. <br>
     * <b> M�todos a probar: </b> <br>
     * listaDobleEncadenada <br>
     * darNodoRaiz <br>
     * <b> Casos de prueba: </b> <br>
     * 1. Se construye un �rbol de lista doble. <br>
     */
	public void testListaDobleEncadenada(){
		setupEscenario1();
		
		assertNotNull("La cabeza no fue inicializada o no iene referencia", listaDoble.darCabeza());
		
		//prueba cantidad de nodos
		assertEquals("La lista no tiene la cantiad de nodos esperados", 3, listaDoble.darNumeroElementos());
		
	}
	
	public void testAgregarElementoFinal(){
		setupEscenario2();
		
		listaDoble.agregarElementoFinal("A");
		
		//Verificar que sea la cabeza
		assertEquals("", "A", listaDoble.darCabeza().getHijo());
		
		
		//verifica qu se haya agrgado correctamete
		listaDoble.agregarElementoFinal("B");
		Iterator<String> iter = listaDoble.iterator();
		
		String a = null;
		
		while(iter.hasNext()){
			a = iter.next();
		}
		
		assertEquals("No se ha agregado correctamene el elemento al final", "B", a);		
		
	}
	
	public void testDarElementoPosicion(){
		setupEscenario1();
		
		//Verifica posicion 1
		assertEquals("El elemento no coincide con el esperado", "A", listaDoble.darElemento(0));
		//Verifica posicion 2
		assertEquals("El elemento no coincide con el esperado", "B", listaDoble.darElemento(1));
		//Verifica posicion 3
		assertEquals("El elemento no coincide con el esperado", "C", listaDoble.darElemento(2));
		
	}
	
	public void testDarCantiadadNodos(){
		setupEscenario1();
		
		assertEquals(3, listaDoble.darNumeroElementos());
		
		listaDoble.agregarElementoFinal("D");
		
		assertEquals(4, listaDoble.darNumeroElementos());
	}
	
	public void testDarElemento(){
		setupEscenario1();
		
		
		assertEquals("", "A", listaDoble.darElementoPosicionActual());
		
		//Prueb para dar el siguiente elemnto 
		if(listaDoble.avanzarSiguientePosicion()){
			assertEquals("", "B", listaDoble.darElementoPosicionActual());
		}else{
			fail("Tenia siguiente elemento");
		}
		
		listaDoble.agregarElementoFinal("D");
		
		
		listaDoble.avanzarSiguientePosicion();
		listaDoble.avanzarSiguientePosicion();
		
		if(listaDoble.retrocederPosicionAnterior()){
			String a = listaDoble.darElementoPosicionActual();
			assertEquals("", "C", listaDoble.darElementoPosicionActual());
		}else{
			fail("Tenia elemento anteriro");
		}
		
	}
	
	// Las istas encadenadas se deben probar con string
	// Recordarle a mi coma�ero que cambie l nodo sencillo porque me tir� error, admeas del metodo del iteratot
	
	
	
}
