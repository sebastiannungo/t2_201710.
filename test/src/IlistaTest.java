import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;


public class IlistaTest extends TestCase{
	private ILista listaEncadenada;
	private ILista listaDobleEncadenada;
	private void setupEscenario1()
	{
		listaEncadenada=new ListaEncadenada<String>();
		listaEncadenada.agregarElementoFinal("a");
		listaEncadenada.agregarElementoFinal("b");
		listaEncadenada.agregarElementoFinal("c");
	}
	private void setupEscenario2()
	{
		listaDobleEncadenada = new ListaDobleEncadenada();
		listaDobleEncadenada.agregarElementoFinal("a");
		listaDobleEncadenada.agregarElementoFinal("b");
		listaDobleEncadenada.agregarElementoFinal("c");
	}
	public void testEncadenadaDarElementoPosicionActual()
	{
		setupEscenario1();
		assertEquals("No es la pocicion actual", "a", listaEncadenada.darElementoPosicionActual());
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("No es la pocicion actual", "b", listaEncadenada.darElementoPosicionActual());
		
	}
	public void testDobleEncadenadaDarElementoPosicionActual()
	{
		setupEscenario2();
		assertEquals("No es la pocicion actual", "a", listaDobleEncadenada.darElementoPosicionActual());
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("No es la pocicion actual", "b", listaDobleEncadenada.darElementoPosicionActual());
	}
	public void testEncadenadaavanzarSiguientePosicion()
	{
		setupEscenario1();
		assertTrue("no avanzo de posicion", listaEncadenada.avanzarSiguientePosicion());
		assertEquals("No avanzo de pocicion", "b", listaEncadenada.darElementoPosicionActual());
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("No es la pocicion actual", "c", listaEncadenada.darElementoPosicionActual());
		
	}
	public void testDobleEncadenadaavanzarSiguientePosicion()
	{
		setupEscenario2();
		assertTrue("no avanso de posicion", listaDobleEncadenada.avanzarSiguientePosicion());
		assertEquals("No avanzo de pocicion", "b", listaDobleEncadenada.darElementoPosicionActual());
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertEquals("No es la pocicion actual", "c", listaDobleEncadenada.darElementoPosicionActual());
		assertFalse("no es posible avansar", listaDobleEncadenada.avanzarSiguientePosicion());
	}
	public void testEncadenadaretrocederPosicionAnterior()
	{
		setupEscenario1();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		assertTrue("no se pudo retoceder", listaEncadenada.retrocederPosicionAnterior());
		assertEquals("No retrocedio de pocicion", "b", listaEncadenada.darElementoPosicionActual());
		assertTrue("no se pudo retoceder", listaEncadenada.retrocederPosicionAnterior());
		assertEquals("No retrocedio de pocicion", "a", listaEncadenada.darElementoPosicionActual());
		assertFalse("No deberia retroceder", listaEncadenada.retrocederPosicionAnterior());
		
	}
	public void testDobleEncadenadaretrocederPosicionAnterior()
	{
		setupEscenario2();
		listaDobleEncadenada.avanzarSiguientePosicion();
		listaDobleEncadenada.avanzarSiguientePosicion();
		assertTrue("no se pudo retoceder", listaDobleEncadenada.retrocederPosicionAnterior());
		assertEquals("No retrocedio de pocicion", "b", listaDobleEncadenada.darElementoPosicionActual());
		assertTrue("no se pudo retoceder", listaDobleEncadenada.retrocederPosicionAnterior());
		assertEquals("No retrocedio de pocicion", "a", listaDobleEncadenada.darElementoPosicionActual());
		assertFalse("No deberia retroceder", listaDobleEncadenada.retrocederPosicionAnterior());
		
	}
}
