package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> cabeza;

	private int size;
	
	private NodoDoble<T> navegador;

	public ListaDobleEncadenada(){
		cabeza = null;
		size = 0;
		navegador = cabeza;
	}
	
	public NodoDoble<T> darCabeza(){
		return cabeza;
	}
	
	public int size(){
		return size;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>(){
			NodoDoble<T> act = null;

			public boolean hasNext(){
				if (size == 0) return false;
				if (act == null) return true;

				return act.getSiguiente() != null;
			}

			public T next(){
				if (act == null){
					act = cabeza;
				}
				else{
					act = act.getSiguiente();
				}

				return (T) act.getHijo();
			}

		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nNode = new NodoDoble<T>(elem);

		if(cabeza == null){
			cabeza = nNode;
			navegador = cabeza;
			size++;
		}

		else{
			NodoDoble<T> actual = cabeza;
			while(actual.getSiguiente() != null){
				actual = actual.getSiguiente();
			}
			nNode.setAnterior(actual);
			actual.setSiguiente(nNode);
			nNode.setAnterior(actual);
		}

		size++;

	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		NodoDoble<T> n = cabeza;
		
		for (int i = 0; i < pos; i++) {
			n = n.getSiguiente();
		}
				
		return (T) n.getHijo();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		
		return (T) navegador.getHijo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if (navegador.getSiguiente() == null) return false;
		else {
			navegador = navegador.getSiguiente();
			return true;
		}
		

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if (navegador == cabeza) return false;
		else {
			navegador = navegador.getAnterior();
			return true;
		}
	}

}
