package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;

import model.data_structures.ListaEncadenada;

import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.util.ArrayList;
import java.util.Iterator;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	private VOAgnoPelicula posicion = null;

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		File archivo = new File(archivoPeliculas);	
		try {
			FileReader lector = new FileReader(archivo);
			BufferedReader buffer = new BufferedReader(lector);
			
			String linea;
	        
			while((linea=buffer.readLine())!=null)
	        {	
	        	String[] texto = linea.split(",");
	        	if(texto[1]=="title"){linea=buffer.readLine();}
	        	
	        	String[] par = linea.split(",");
	        	VOPelicula pelicula = new VOPelicula();
	        	
	        	if(par[1]!=null){String[] id = par[1].split("(" +" "+ ")");int agno=0;
	        	String titulo=id[0];
	        	
	        	
	        	String[] generos = par[2].split("|");
	        	ListaEncadenada listaGeneros=new ListaEncadenada<String>();
	        	
	        	pelicula.setAgnoPublicacion(agno);
	        	pelicula.setTitulo(titulo);
	        	
	        	for (int i = 0; i < listaGeneros.darElementos(); i++) {
					listaGeneros.agregarElementoFinal(generos[i]);
				}
	        	pelicula.setGenerosAsociados(listaGeneros);
	        	
	        	misPeliculas.agregarElementoFinal(pelicula);
	        }
	        }
	        lector.close();
	        buffer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(VOPelicula peli:misPeliculas)
		{
			if(existeAgno(peli.getAgnoPublicacion()))
			{
			avansarAgno(peli.getAgnoPublicacion());
			peliculasAgno.darElementoPosicionActual().getPeliculas().agregarElementoFinal(peli);
			}
			else
			{
				VOAgnoPelicula pelisAgno = new VOAgnoPelicula();
				pelisAgno.setAgno(peli.getAgnoPublicacion());
				ListaDobleEncadenada lista = new ListaDobleEncadenada();
				lista.agregarElementoFinal(peli);
				pelisAgno.setPeliculas(lista);
				peliculasAgno.agregarElementoFinal(pelisAgno);
			}
		}
		
		}
		
	
	
	private void avansarAgno(int agnoPublicacion) {
		while(peliculasAgno.darElementoPosicionActual().getAgno()!=agnoPublicacion)
		{
			peliculasAgno.avanzarSiguientePosicion();
		}
	}



	private boolean existeAgno(int agnoPublicacion) {
		while(peliculasAgno.avanzarSiguientePosicion())
		{
		if(peliculasAgno.darElementoPosicionActual().getAgno()==agnoPublicacion)return true;
		else 
			peliculasAgno.avanzarSiguientePosicion();
		}
		return false;
	}



	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		ILista<VOPelicula> respuesta = new ListaDobleEncadenada<>();
		Iterator<VOPelicula> iter = misPeliculas.iterator();

		while(iter.hasNext()){
			VOPelicula add = iter.next();
			if(add.getTitulo().matches("(.*)"+busqueda+"(.*)"))
				respuesta.agregarElementoFinal(add);
		}


		// TODO Auto-generated method stub
		return respuesta;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		ILista<VOPelicula> respuesta = null;
		Iterator<VOAgnoPelicula> iter = peliculasAgno.iterator();

		while(iter.hasNext()){
			VOAgnoPelicula posible = iter.next();
			if(posible.getAgno() == agno){
				respuesta = posible.getPeliculas();
				break;
			}
		}

		// TODO Auto-generated method stub
		return respuesta;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		VOAgnoPelicula respuesta = null;
		if(peliculasAgno.avanzarSiguientePosicion())
			respuesta = peliculasAgno.darElementoPosicionActual();
			
		return respuesta;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		VOAgnoPelicula respuesta = null;
		if(peliculasAgno.retrocederPosicionAnterior())
			respuesta = peliculasAgno.darElementoPosicionActual();
		// TODO Auto-generated method stub
		return respuesta;
	}

}
